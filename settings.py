# Example data. Change as needed
TEAM_CALENDAR_ID = 'muehlemann-popp.ch_2cl0ivovjr83c2u5qm9iop7t4c@group.calendar.google.com'
PERSONAL_CALENDAR_ID = 'silvan.muehlemann@muehlemann-popp.ch'


# created at https://console.developers.google.com/apis/credentials?project=meetingroom-kiosk&hl=en&organizationId=758695447740
CLIENT_SECRET_KEY = 'meetingroom-kiosk-390d7d04b351.json'
# see https://developers.google.com/identity/protocols/OAuth2ServiceAccount
USER_TO_IMPERSONATE = 'silvan.muehlemann@muehlemann-popp.ch'
