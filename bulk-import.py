"""Creates recurring calendar entries for work anniversaries and
birthdays from a CSV"""
import csv
import datetime

from connector import CalendarClient, create_events
from settings import *

calendar_client = CalendarClient(CLIENT_SECRET_KEY, USER_TO_IMPERSONATE)

header = True
with open('anniversaries-2.csv', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=";")

    for row in reader:
        if header:
            header = False
            continue
        first_day_of_work = datetime.datetime.strptime(row[13], "%d.%m.%Y")
        birthday = datetime.datetime.strptime(row[7], "%d.%m.%Y")
        create_events(f"{row[1]} {row[3]}", first_day_of_work=first_day_of_work, birthday=birthday,
                      create_work_anniversary=False, create_birthday=False, create_probation_period=False,
                      create_half_year_review=True)
