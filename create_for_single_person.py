#!/usr/bin/env python3

"""Creates recurring calendar entries for work anniversaries and
birthdays from calendar"""
import argparse
import datetime
from pprint import pprint

from settings import *

from connector import CalendarClient, create_events

parser = argparse.ArgumentParser(description='Create calendar entries for HR tasks')
parser.add_argument('name', help='Name of the team member')
parser.add_argument('first_day_of_work', help='First day of work, format "d.m.Y"')
parser.add_argument('birthday', help='Birthday, format "d.m.Y"')
parser.add_argument('-w', '--workanniversary', action='store_true', help="Create work anniversary reminders")
parser.add_argument('-b', '--birthdays', action='store_true', help="Create birthday reminders")
parser.add_argument('-p', '--probationperiod', action='store_true', help="Create probation period reminders")
parser.add_argument('-h', '--halfyear', action='store_true', help="Create half-year-review reminders")
args = parser.parse_args()

first_day_of_work = datetime.datetime.strptime(args.first_day_of_work, "%d.%m.%Y")
birthday = datetime.datetime.strptime(args.birthday, "%d.%m.%Y")
create_events(args.name, first_day_of_work=first_day_of_work, birthday=birthday,
              create_work_anniversary=args.workanniversary, create_birthday=args.birthdays, create_probation_period=args.probationperiod)
