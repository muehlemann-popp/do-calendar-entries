import datetime
import os
from pprint import pprint

import httplib2
from googleapiclient import discovery
from oauth2client.service_account import ServiceAccountCredentials
from settings import *


class CalendarClient:
    def __init__(self, client_secret_key_file: str, user_to_impersonate: str):
        scopes = ['https://www.googleapis.com/auth/calendar']
        dir_path = os.path.dirname(os.path.realpath(__file__))
        credentials = ServiceAccountCredentials.from_json_keyfile_name(
            dir_path + '/etc/' + client_secret_key_file, scopes=scopes)
        delegated_credentials = credentials.create_delegated(
            user_to_impersonate)
        http_auth = delegated_credentials.authorize(httplib2.Http())
        self.service = discovery.build('calendar', 'v3', http_auth)

    def get_service(self):
        return self.service


def create_yearly_event(date, title, calendar_id, is_yearly=False):
    event = {
        'summary': title,
        'description': datetime.datetime.strftime(date, "%d.%m.%Y"),
        'start': {
            'date': datetime.datetime.strftime(date, "%Y-%m-%d"),
            'timeZone': 'Europe/Berlin',
        },
        'end': {
            'date': datetime.datetime.strftime(date, "%Y-%m-%d"),
            'timeZone': 'Europe/Berlin',
        },
        'reminders': {
            'useDefault': False,
            'overrides': [
                {'method': 'email', 'minutes': 7 * 24 * 60},
                {'method': 'popup', 'minutes': 1 * 24 * 60},
            ],
        },
    }

    if is_yearly:
        event['recurrence'] = ['RRULE:FREQ=YEARLY']

    print(event)

    event = calendar_client.get_service().events().insert(
        calendarId=calendar_id, body=event).execute()
    print('Event created: %s, %s' % (title, datetime.datetime.strftime(date, "%Y-%m-%d")))


def create_events(name, first_day_of_work, birthday, create_work_anniversary=False, create_birthday=False,
                  create_probation_period=False, create_half_year_review=False):
    pprint(f'Create events for {name}')
    if create_work_anniversary:
        create_yearly_event(first_day_of_work, f"{name}, Work Anniversary ", PERSONAL_CALENDAR_ID, is_yearly=True)
    if create_birthday:
        create_yearly_event(birthday, f"{name}, Birthday ", PERSONAL_CALENDAR_ID, is_yearly=True)
    if create_probation_period:
        create_yearly_event(first_day_of_work + datetime.timedelta(days=45), f"{name}, Peer Review 1/2",
                            PERSONAL_CALENDAR_ID)
        create_yearly_event(first_day_of_work + datetime.timedelta(days=90), f"{name}, End of probation period",
                            PERSONAL_CALENDAR_ID)
    if create_half_year_review:
        create_yearly_event(first_day_of_work + datetime.timedelta(days=364/2), f"{name}, Half Year Review",
                            PERSONAL_CALENDAR_ID)

calendar_client = CalendarClient(CLIENT_SECRET_KEY, USER_TO_IMPERSONATE)
