Do-calendar-entries
===================

Create calendar entries in your Google Calendar for 

- Birthdays
- Work anniversaries
- Peer Reviews 

### Setup

Create a project in Google Console. Activate Calendar API. Store json files with credentials in etc/

### Usage:

For a single team member:

    ./create_for_single_person.py -w -b -p "Zakhar Hauryliuk" "04.06.2018" "01.11.1988"

For a set of team members:

* Export the file from Peopleforce
* Save it as CSV
* Run `bulk-import.py`

